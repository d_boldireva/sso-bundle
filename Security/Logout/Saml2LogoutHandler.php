<?php

namespace TeamSoft\SsoBundle\Security\Logout;

use OneLogin\Saml2\Error;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use TeamSoft\SsoBundle\Security\Authentication\Saml2Auth;
use TeamSoft\SsoBundle\Security\Authentication\Token\Saml2Token;

class Saml2LogoutHandler implements LogoutHandlerInterface
{
    protected $auth;

    public function __construct(Saml2Auth $auth)
    {
        $this->auth = $auth;
    }

    public function logout(Request $request, Response $response, TokenInterface $token = null)
    {
        if ($token instanceof Saml2Token) {
            try {
                $this->auth->processSLO();
            } catch (Error $e) {
                if (!empty($this->auth->getSLOurl())) {
                    $sessionIndex = $token->hasAttribute('sessionIndex') ? $token->getAttribute('sessionIndex') : null;
                    $this->auth->logout(null, [], $token->getUsername(), $sessionIndex);
                }
            }
        }
    }
}
