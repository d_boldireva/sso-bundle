<?php

namespace TeamSoft\SsoBundle\Security\Authentication;

use OneLogin\Saml2\Auth;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\RouterInterface;

class Saml2Auth extends Auth
{
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag, RouterInterface $router)
    {
        $this->parameterBag = $parameterBag;

        if ($saml2Config = $this->parameterBag->get('sso.saml2')) {
            try {
                $saml2Config['sp']['entityId'] = $router->generate($saml2Config['sp']['entityId'], [], RouterInterface::ABSOLUTE_URL);
            } catch (RouteNotFoundException $e) {}
            try {
                $saml2Config['sp']['assertionConsumerService']['url'] = $router->generate($saml2Config['sp']['assertionConsumerService']['url'], [], RouterInterface::ABSOLUTE_URL);
            } catch (RouteNotFoundException $e) {}
            try {
                $saml2Config['sp']['singleLogoutService']['url'] = $router->generate($saml2Config['sp']['singleLogoutService']['url'], [], RouterInterface::ABSOLUTE_URL);
            } catch (RouteNotFoundException $e) {}
            parent::__construct($saml2Config);
        }
    }

    public function login(
        $returnTo = null,
        array $parameters = [],
        $forceAuthn = false,
        $isPassive = false,
        $stay = false,
        $setNameIdPolicy = true,
        $nameIdValueReq = null
    ): ?string
    {
        if ($saml2Config = $this->parameterBag->get('sso.saml2')) {
            return parent::login($returnTo, $parameters, $forceAuthn, $isPassive, $stay, $setNameIdPolicy, $nameIdValueReq);
        } else {
            return null;
        }
    }
}
