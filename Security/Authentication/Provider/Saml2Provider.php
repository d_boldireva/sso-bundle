<?php

namespace TeamSoft\SsoBundle\Security\Authentication\Provider;

use Doctrine\ORM\EntityManagerInterface;
use foo\bar;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\SsoBundle\Security\Authentication\Token\Saml2Token;

class Saml2Provider implements AuthenticationProviderInterface
{

    private $userProvider;
    private $userChecker;

    public function __construct(UserProviderInterface $userProvider,
                                UserCheckerInterface $userChecker
    )
    {
        $this->userProvider = $userProvider;
        $this->userChecker = $userChecker;
    }

    public function supports(TokenInterface $token): bool
    {

        return $token instanceof Saml2Token;
    }

    public function authenticate(TokenInterface $token): TokenInterface
    {
        if (!$this->supports($token)) {
            throw new AuthenticationException('The token is not supported by this authentication provider.');
        }

        $user = $this->retrieveUser($token->getUsername());

        $this->userChecker->checkPreAuth($user);
        $this->userChecker->checkPostAuth($user);

        $authenticatedToken = new Saml2Token($user->getRoles());
        $authenticatedToken->setUser($user);
        $authenticatedToken->setAttributes($token->getAttributes());
        $authenticatedToken->setAuthenticated(true);

        return $authenticatedToken;
    }

    private function retrieveUser($username)
    {

        $user = $this->userProvider->loadUserByUsername($username);

        if (null === $user) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }

        return $user;
    }
}
