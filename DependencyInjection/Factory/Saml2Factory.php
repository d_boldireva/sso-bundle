<?php

namespace TeamSoft\SsoBundle\DependencyInjection\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use TeamSoft\SsoBundle\Security\Authentication\Provider\Saml2Provider;
use TeamSoft\SsoBundle\Security\Authentication\Saml2Auth;
use TeamSoft\SsoBundle\Security\Firewall\Saml2Listener;

class Saml2Factory extends AbstractFactory
{
    public function __construct()
    {
        $this->addOption('username_attribute');
        $this->addOption('user_provider');
    }

    public function getPosition(): string
    {
        return 'pre_auth';
    }

    public function getKey(): string
    {
        return 'saml2';
    }

    protected function getListenerId(): string
    {
        return Saml2Listener::class;
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId): string
    {
        $provider = 'security.authentication.provider.saml2.' . $id;
        if (isset($config['user_provider'])) {
            $userProviderId = 'security.user.provider.concrete.' . $config['user_provider'];
        }
        $container
            ->setDefinition($provider, new ChildDefinition(Saml2Provider::class))
            ->addArgument(new Reference($userProviderId))
            ->addArgument(new Reference('security.user_checker.' . $id));
        return $provider;
    }

    /**
     * @param ContainerBuilder $container
     * @param $id
     * @param $config
     * @param $userProvider
     * @return string
     */
    protected function createListener($container, $id, $config, $userProvider)
    {
        $listenerId = parent::createListener($container, $id, $config, $userProvider);
        $defenition = $container->getDefinition($this->getListenerId() . '.' . $id);
        $defenition->addArgument(new Reference(Saml2Auth::class));
        return $listenerId;
    }
}
