<?php
namespace TeamSoft\SsoBundle\DependencyInjection\Factory;

use OneLogin\Saml2\Auth;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;

class Saml2AuthFactory
{
    public function create (ContainerInterface $container, RouterInterface $router) {
        $saml2Config = $container->getParameter('sso.saml2');
        $saml2Config['sp']['entityId'] = $router->generate($saml2Config['sp']['entityId'], [], RouterInterface::ABSOLUTE_URL);
        $saml2Config['sp']['assertionConsumerService']['url'] = $router->generate($saml2Config['sp']['assertionConsumerService']['url'], [], RouterInterface::ABSOLUTE_URL);
        $saml2Config['sp']['singleLogoutService']['url'] = $router->generate($saml2Config['sp']['singleLogoutService']['url'], [], RouterInterface::ABSOLUTE_URL);
        return new Auth($saml2Config);
    }
}
