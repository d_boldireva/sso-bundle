<?php

namespace TeamSoft\SsoBundle\DependencyInjection;

//use Doctrine\DBAL\Connection;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
//use Symfony\Component\Config\FileLocator;
//use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
//use Symfony\Component\DependencyInjection\Loader;
//use TeamSoft\CrmRepositoryBundle\Utils\ExtensionTrait;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class TeamSoftSsoExtension extends Extension //implements PrependExtensionInterface
{

//    private $webServices;
//
//    public function __construct(array $webServices)
//    {
//        $this->webServices = $webServices;
//    }
//
//    public function getAlias()
//    {
//        return 'db';
//    }

//    public function prepend(ContainerBuilder $container)
//    {
//        $container->loadFromExtension($this->getAlias(), ['web_services' => $this->webServices]);
//        $configs = $container->getExtensionConfig($this->getAlias());
//        $configuration = new Configuration();
//        $config = $this->processConfiguration($configuration, $configs);
//
//        foreach ($config['web_services'] as $serviceName => $serviceParameters) {
//            $prefix = "db.web_services.$serviceName";
//            foreach ($serviceParameters as $parameterName => $parameterValue) {
//                if (in_array($parameterName, ['allow_if'])) {
//                    $container->setParameter("$prefix.$parameterName", $parameterValue);
//                }
//            }
//        }
//    }

    public function getAlias(): string
    {
        return 'sso';
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('sso.saml2', $config['saml2'] ?? null);
    }
}
