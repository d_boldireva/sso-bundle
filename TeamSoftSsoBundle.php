<?php

namespace TeamSoft\SsoBundle;

use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

use TeamSoft\SsoBundle\DependencyInjection\Factory\Saml2Factory;
use TeamSoft\SsoBundle\DependencyInjection\TeamSoftSsoExtension;

class TeamSoftSsoBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        /**
         * @var SecurityExtension $extension
         */
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new Saml2Factory());
    }

    public function getContainerExtension()
    {
        return new TeamSoftSsoExtension();
    }
}
